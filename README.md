# johnny decimal

Scripts related to creating or maintaining a [Johnny Decimal](https://johnnydecimal.com/) system

## Ideas

- A *Very Fancy* script that determines the next available ID number based on supplied category and adds your new item to the Index at the same time

## Existing Tools

### build-index.sh

This script loops over your index directory system and builds a set of markdown files - one for each area. 

The markdown files are formatted such that they can be directly dumped into a Hugo (or other SSG) instance 

- so you can publish your index on a static Pages host 
    - so you can then determine the next ID number even when your filing system isn't accessible. 
    
Setting your Pages site as "not public" is probably wise - don't give away OSINT for free!

This script will run in $PWD unless you optionally specify the root of your JD directory.

You may also optionally specify the outfile, but if you choose to do this, you must specify the root of your JD directory.

Usage:

```sh
build-index.sh [/path/to/JD/root] [/outfile/path/]
```
===

You may contribute if you wish. Merge requests are welcomed. Don't be a jerk, &c &c.

Contributors:

- [Emma Tebibyte](https://gitlab.com/emmatebibyte)
